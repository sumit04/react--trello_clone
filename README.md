# TRELLO CLONE with REACT JS

[![mozaïk channel on discord](https://img.shields.io/badge/NODE-REACT-61dafb.svg?style=flat-square)](https://discord.gg/E6tQSsQ)

![Repo List](readmeImage/Screenshot.png)

```
Trello clone using ReactJs
```

# Features

```
Display All Boards

Add Multiple Boards
```

```
Add Multiple Lists

Archive Lists

Display all lists in the board
```

```
Display all cards in each list

Add a card to a list

Delete a card from a list
```

```
Add a checklist to a card

Delete a checklist from card

Display all the checklist in each card
```

```
Display all checklist items in each checklists

Add a checklists items to a checklist

Delete a checklist items from a cheklist

Update a checklist items in a checklist
```

```
Responsive:
        -Mobile
        -Tablet
        -Large Screen
```

## Getting Started for your own REACT app

The quickest way to get started is to use `create-react-project`.

```
npm install -g create-react-project
create-react-project the-best-app-ever
cd the-best-app-ever
npm install
npm start
```

Now open [http://localhost:8080](http://localhost:8080).

Go edit a file, notice the app reloads

## Cloning and Running this Application in local

**Clone the project into local**

```
using-

git clone https://gitlab.com/sumit04/react--trello_clone.git

```

Install all the npm packages. Go into the project folder and type the following command to install all npm packages

```bash
npm install
```

In order to run the application Type the following command

```bash
npm start
```

The Application Runs on **localhost:3000**

## Tools and Languages

```
REACT JS
```

```
JSX
```

```
BootStrap
```

```
CSS
```

```
STYLED COMPONENTS
```

```
Rest of the tools are already in CREATE-REACT-APP
```

## Resources

**create-react-app** : The following link has all the commands that can be used with create-react-app
https://github.com/facebook/create-react-app

**ReactJS** : Refer to https://reactjs.org/ to understand the concepts of ReactJS

**Font-Awsesome**

## Authors

- **Sumit Aswal**
