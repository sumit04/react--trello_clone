import React, { Component } from 'react';
import List from './List';
import styled from 'styled-components';
import { DataConsumer } from '../context';
import InputFiled from './inputFiled';

export default class Board extends Component {
  render() {
    return (
      <DataConsumer>
        {value => {
          return (
            <BoardWrapper>
              {/* <h3 className='board-name'>Sumit's board</h3> */}
              <div className='list-wrapper'>
                <List />
                <div className='button-add-list'>
                  {value.inputShowList ? (
                    <InputFiled name={'list'} keyPressed={value.keyPressed} />
                  ) : null}

                  <span data-type='list' onClick={() => value.showInput(event)}>
                    + Add a List{' '}
                  </span>
                </div>
              </div>
            </BoardWrapper>
          );
        }}
      </DataConsumer>
    );
  }
}
const BoardWrapper = styled.div`
  margin-top: 5em;
  // height: 100%;
  .list-wrapper {
    display: flex;
    padding: 2em 0 0 1em;
  }
  // .board-name {
  //   background-color: #026aa7;
  //   text-align: center;
  //   color: white;
  //   padding-top: 0.5em;
  // }
  .button-add-list {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    min-width: 20em;
    height: fit-content;
    padding: 0.5em;
    background-color: #026aa7;
    color: white;
    border-radius: 0.3em;
    word-spacing: 0.1em;
    letter-spacing: 0.08em;
  }
`;
