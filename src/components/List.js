import React, { Component } from 'react';
import styled from 'styled-components';
import Card from './Card';
import { DataConsumer } from '../context';
import InputFiled from './inputFiled';

export default class List extends Component {
  render() {
    return (
      <DataConsumer>
        {value => {
          return value.lists.map(list => {
            return (
              <CardConatiner key={list.id}>
                <div className='list-heading'>
                  <h5 className='heading'>{list.name}</h5>
                  <span onClick={() => value.archiveList(list.id)}>X</span>
                </div>
                <div className='cards-wrapper'>
                  <Card
                    cards={list.cards}
                    isShowing={value.isShowing}
                    getCardsDetails={value.getCardsDetails}
                    openModalHandler={value.openModalHandler}
                    closeModalHandler={value.closeModalHandler}
                    deleteCard={value.deleteCard}
                    listID={list.id}
                  />
                </div>
                <div className='button-add-card'>
                  {value.inputShow && value.listID === list.id ? (
                    <InputFiled name={'card'} keyPressed={value.keyPressed} />
                  ) : null}

                  <span
                    data-id={list.id}
                    data-type={'card'}
                    onClick={() => value.showInput(event)}
                  >
                    + Add Card
                  </span>
                </div>
              </CardConatiner>
            );
          });
        }}
      </DataConsumer>
    );
  }
}

const CardConatiner = styled.div`
  min-width: 20em;
  padding: 1em;
  background-color: #ebecf0;
  color: black;
  border-radius: 0.3em;
  margin-right: 2em;
  height: fit-content;
  .heading {
    margin-left: 0.6em;
  }
  .button-add-card {
    // margin-top: 2em;
    color: grey;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }
  .cards-wrapper {
    overflow: auto;
    max-height: 70vh;

    ::-webkit-scrollbar {
      width: 8px;
    }

    ::-webkit-scrollbar-track {
      box-shadow: none;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: grey;
      border-radius: 10px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: black;
    }
  }

  .list-heading {
    display: flex;
    justify-content: space-between;
  }
`;
