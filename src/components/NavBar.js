import React from 'react';
import logo from '../../public/images/header-logo-2x.01ef898811a879595cea.png';
import { DataConsumer } from '../context';
export default function NavBar() {
  return (
    <nav
      className='navbar navbar-expand-bg navbar-dark fixed-top'
      style={{ backgroundColor: '#026aa7' }}
    >
      <a className='navbar-brand' href='#'>
        <i className='fas fa-home' />
      </a>

      <img src={logo} alt='logo' style={{ blockSize: '2em' }} />
      <button
        className='navbar-toggler'
        type='button'
        data-toggle='collapse'
        data-target='#navbarNavAltMarkup'
        aria-controls='navbarNavAltMarkup'
        aria-expanded='false'
        aria-label='Toggle navigation'
      >
        <span className='navbar-toggler-icon'></span>
      </button>
      <div className='collapse navbar-collapse' id='navbarNavAltMarkup'>
        <div className='navbar-nav'>
          <DataConsumer>
            {value => {
              return value.boards.map(board => {
                return (
                  <a
                    key={board.id}
                    className='nav-item nav-link active'
                    onClick={() => value.getBoardDetails(board.id)}
                    style={{ cursor: 'pointer' }}
                  >
                    {board.name}
                  </a>
                );
              });
            }}
          </DataConsumer>
        </div>
      </div>
    </nav>
  );
}
