import React from 'react';
// import ReactReponsiveModal from 'react-responsive-modal/types';

export default function CheckItem(props) {
  const checkItems = props.list;
  return (
    // mapping and creating checkItems
    <React.Fragment>
      {checkItems.map(checkItem => (
        <div className=' modal-body input-group ' key={checkItem.id}>
          <div className='input-group-prepend'>
            <div className='input-group-text'>
              <input
                type='checkbox'
                name='checkbox'
                defaultChecked={checkItem.state === 'complete' ? true : false}
                onChange={() => props.updateCheckItem(checkItem.id)}
              />
            </div>
          </div>
          <div className='form-control'>{checkItem.name}</div>

          <div className='input-group-append'>
            <button
              className='btn btn-secondary'
              type='submit'
              onClick={() =>
                props.deleteCheckItem(checkItem.id, props.checkListID)
              }
            >
              &minus;
            </button>
          </div>
        </div>
      ))}
    </React.Fragment>
  );
}
