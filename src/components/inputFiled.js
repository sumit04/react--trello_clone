import React from 'react';

export default function InputFiled(prop) {
  return (
    <input
      className={`input-${prop.name}`}
      type='text'
      name={`${prop.name}`}
      placeholder={`Enter ${prop.name} Name`}
      // onChange={value.myChangeHandler}
      onKeyPress={prop.keyPressed}
    ></input>
  );
}
