import React, { Component } from 'react';
const DataContext = React.createContext();

const REACT_APP_API_KEY = process.env.REACT_APP_API_KEY;
const token = process.env.REACT_APP_TOKEN;
const username = process.env.REACT_APP_USER_NAME;

//provides data to all the consumers
//lists store all the lists and its cards details
//cards store all the checklist and items in cards
//isShowing is for modal popUP
//input-show-[list,check,item] is for input filed, if false then user will not see input field
//listid ,cardid, checklistid store the current id .
class DataProvider extends Component {
  state = {
    boards: [],

    lists: [],
    cards: [],
    isShowing: false,
    inputShow: false,
    inputShowList: false,
    inputShowCheck: false,
    inputShowItem: false,
    input: '',
    currentBoaredID: '',
    cardName: '',
    listId: '',
    cardId: '',
    checkListId: ''
  };
  //sets the data to state after first render
  componentDidMount() {
    this.setData();
  }
  setData = async () => {
    const boardJson = await fetch(
      `https://api.trello.com/1/members/${username}?boards=all&lists=all&cards=all&fields=none&key=${REACT_APP_API_KEY}&token=${token}`
    );
    const boardsID = await boardJson.json();
    // console.log('hello', boardData.boards);
    this.setState(
      {
        boards: boardsID.boards
      },
      () => this.getBoardDetails(this.state.boards[4].id)
    );
  };
  //get boards detail based on their id
  getBoardDetails = async id => {
    const listJson = await fetch(
      `https://api.trello.com/1/boards/${id}/lists?fields=name&key=${REACT_APP_API_KEY}&cards=all&card_fields=name&filter=open&token=${token}`
    );
    const listData = await listJson.json();
    this.setState({
      lists: listData,
      currentBoaredID: id
    });
  };
  //get cards detail based on their id
  getCardsDetails = async (id, name) => {
    const cardsJson = await fetch(
      `https://api.trello.com/1/cards/${id}/checklists?checkItems=all&checkItem_fields=name%2Cstate&token=${token}&key=${REACT_APP_API_KEY}`
    );
    const cardData = await cardsJson.json();
    this.setState({
      cardName: name,
      cards: cardData,
      cardId: id
    });
  };
  //handels if modal is open
  openModalHandler = () => {
    this.setState({
      isShowing: true
    });
  };
  //handels if modal is close
  closeModalHandler = () => {
    this.setState({
      isShowing: false
    });
  };
  //delete cards from trello throug api
  deleteCard = async (cardID, listID) => {
    try {
      await fetch(
        `https://api.trello.com/1/cards/${cardID}?key=${REACT_APP_API_KEY}&token=${token}`,
        {
          method: 'delete'
        }
      );
      let tempLists = [...this.state.lists];
      let selectedList = tempLists.find(item => item.id === listID);
      let index = tempLists.indexOf(selectedList);
      let tempCards = selectedList.cards;
      tempCards = tempCards.filter(item => item.id !== cardID);
      selectedList.cards = tempCards;
      tempLists[index] = selectedList;
      this.setState({
        lists: [...tempLists]
      });
    } catch (error) {
      console.log(`error ${error}`);
    }
  };
  //delete lists from trello throug api
  archiveList = async listID => {
    try {
      await fetch(
        `https://api.trello.com/1/lists/${listID}/closed?value=true&key=${REACT_APP_API_KEY}&token=${token}`,
        {
          method: 'PUT'
        }
      );
      let tempLists = [...this.state.lists];
      tempLists = tempLists.filter(item => item.id !== listID);
      this.setState({
        lists: [...tempLists]
      });
    } catch (error) {
      console.log(`error ${error}`);
    }
  };
  //delete CheckLists from trello throug api
  deleteCheckList = async checkListID => {
    try {
      await fetch(
        `https://api.trello.com/1/checklists/${checkListID}?key=${REACT_APP_API_KEY}&token=${token}`,
        {
          method: 'DELETE'
        }
      );
      let tempCheckList = [...this.state.cards];
      tempCheckList = tempCheckList.filter(item => item.id !== checkListID);
      this.setState({
        cards: [...tempCheckList]
      });
    } catch (error) {
      console.log(`error ${error}`);
    }
  };
  //delete CheckItem from trello throug api
  deleteCheckItem = async (checkItemID, checkListID) => {
    try {
      await fetch(
        `https://api.trello.com/1/checklists/${checkListID}/checkItems/${checkItemID}?key=${REACT_APP_API_KEY}&token=${token}`,
        {
          method: 'DELETE'
        }
      );
      let tempCheckList = [...this.state.cards];
      let selectedCheckList = tempCheckList.find(
        item => item.id === checkListID
      );
      let index = tempCheckList.indexOf(selectedCheckList);
      let tempCheckItems = selectedCheckList.checkItems;
      tempCheckItems = tempCheckItems.filter(
        checkItem => checkItem.id !== checkItemID
      );
      selectedCheckList.checkItems = tempCheckItems;
      tempCheckList[index] = selectedCheckList;
      this.setState({
        cards: [...tempCheckList]
      });
    } catch (error) {
      console.log(`error ${error}`);
    }
  };
  showInputHandler = event => {
    // console.log(event);
    const type = event.target.dataset.type;
    //show input fild for card
    if (type === 'card') {
      this.setState({
        inputShow: true,
        listID: event.target.dataset.id
      });
    }
    //show input fild for List
    if (type === 'list') {
      this.setState({
        inputShowList: true
      });
    }
    //show input fild for checkList
    if (type === 'checkList') {
      this.setState({
        inputShowCheck: true
      });
    }
    //show input fild for checkItem
    if (type === 'checkItem') {
      this.setState({
        inputShowItem: true,
        checkListId: event.target.dataset.id
      });
    }
  };

  keyPressedHandler = event => {
    const name = event.target.value;
    const type = event.target.name;
    // console.log(event.target);
    if (event.key === 'Enter' && type === 'card') {
      this.addCardTrello(name).catch({});
    }
    if (event.key === 'Enter' && type === 'list') {
      this.addListTrello(name).catch({});
    }
    if (event.key === 'Enter' && type === 'checkList') {
      this.addCheckListTrello(name).catch({});
    }
    if (event.key === 'Enter' && type === 'checkItem') {
      this.addCheckItemTrello(name).catch({});
    }
  };

  //when ever pressed is enter,then post card to trello
  addCardTrello = async name => {
    try {
      let response = await fetch(
        `https://api.trello.com/1/cards?name=${name}&idList=${this.state.listID}&keepFromSource=all&key=${REACT_APP_API_KEY}&token=${token}`,
        {
          method: 'POST'
        }
      );
      let data = await response.json();
      this.addCard(name, data.id);
    } catch (error) {
      console.log(`error ${error}`);
    }
    this.setState({
      ...this.state,
      inputShow: false,
      input: ''
    });
  };
  //when ever pressed is enter,then post list to trello
  addListTrello = async name => {
    try {
      let response = await fetch(
        `https://api.trello.com/1/lists?name=${name}&idBoard=${this.state.currentBoaredID}&pos=bottom&key=${REACT_APP_API_KEY}&token=${token}`,
        {
          method: 'POST'
        }
      );
      let data = await response.json();
      this.addList(name, data.id);
    } catch (error) {
      console.log(`error ${error}`);
    }
    this.setState({
      inputShowList: false,
      input: ''
    });
  };
  //when ever pressed is enter,then post checkList to trello
  addCheckListTrello = async name => {
    try {
      let response = await fetch(
        `https://api.trello.com/1/checklists?idCard=${this.state.cardId}&name=${name}&key=${REACT_APP_API_KEY}&token=${token}`,
        {
          method: 'POST'
        }
      );
      let data = await response.json();
      this.addCheckList(name, data.id);
    } catch (error) {
      console.log(`error ${error}`);
    }

    this.setState({
      inputShowCheck: false,
      input: ''
    });
  };
  //when ever pressed is enter,then post checkItem to trello
  addCheckItemTrello = async name => {
    try {
      let response = await fetch(
        `https://api.trello.com/1/checklists/${this.state.checkListId}/checkItems?name=${name}&pos=bottom&checked=false&key=${REACT_APP_API_KEY}&token=${token}`,
        {
          method: 'POST'
        }
      );
      let data = await response.json();
      this.addCheckItems(name, data.id);
    } catch (error) {
      console.log(`error ${error}`);
    }

    this.setState({
      inputShowItem: false,
      input: ''
    });
  };
  // myChangeHandler = event => {
  //   this.setState({ input: event.target.value });
  // };
  addCard = (cardName, cardID) => {
    // console.log(cardName, cardID);
    let tempLists = [...this.state.lists];
    let selectedList = tempLists.find(item => item.id === this.state.listID);
    let index = tempLists.indexOf(selectedList);
    selectedList.cards = [
      ...selectedList.cards,
      { id: cardID, name: cardName }
    ];
    tempLists[index] = selectedList;
    this.setState({
      ...this.state,
      lists: tempLists
    });
  };
  // add lists in boards
  addList = (listName, listId) => {
    // console.log(listName, listId);
    let tempLists = [...this.state.lists];
    tempLists = [
      ...tempLists,
      {
        id: listId,
        name: listName,
        cards: Array(0)
      }
    ];
    this.setState({
      lists: tempLists
    });
    // console.log(tempLists);
  };
  // add checklists in boards
  addCheckList = (checkListName, checkListId) => {
    let tempCheckLists = [...this.state.cards];
    tempCheckLists = [
      ...tempCheckLists,
      {
        id: checkListId,
        name: checkListName,
        checkItems: Array(0)
      }
    ];
    this.setState({
      cards: tempCheckLists
    });
  };
  // add checkitems in boards
  addCheckItems = (checkitemName, checkitemId) => {
    let tempCheckLists = [...this.state.cards];
    let selectedCheckList = tempCheckLists.find(
      item => item.id === this.state.checkListId
    );
    let index = tempCheckLists.indexOf(selectedCheckList);
    selectedCheckList.checkItems = [
      ...selectedCheckList.checkItems,
      { id: checkitemId, name: checkitemName }
    ];
    tempCheckLists[index] = selectedCheckList;
    this.setState({
      cards: tempCheckLists
    });
  };
  //updates on checked and unchecked fields
  updateCheckItem = async checkItemID => {
    let state = event.target.checked;
    try {
      await fetch(
        `https://api.trello.com/1/cards/${this.state.cardId}/checkItem/${checkItemID}?state=${state}&key=${REACT_APP_API_KEY}&token=${token}`,
        {
          method: 'PUT'
        }
      );
    } catch (error) {
      console.log('error ' + error);
    }
  };
  render() {
    return (
      <DataContext.Provider
        value={{
          ...this.state,
          getBoardDetails: this.getBoardDetails,
          getCardsDetails: this.getCardsDetails,
          openModalHandler: this.openModalHandler,
          closeModalHandler: this.closeModalHandler,
          deleteCard: this.deleteCard,
          archiveList: this.archiveList,
          deleteCheckItem: this.deleteCheckItem,
          deleteCheckList: this.deleteCheckList,
          showInput: this.showInputHandler,
          keyPressed: this.keyPressedHandler,
          myChangeHandler: this.myChangeHandler,
          updateCheckItem: this.updateCheckItem
        }}
      >
        {this.props.children}
      </DataContext.Provider>
    );
  }
}

const DataConsumer = DataContext.Consumer;
export { DataProvider, DataConsumer };
