import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
// import { BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import { DataProvider } from './/context';
// require('dotenv').config();
ReactDOM.render(
  <DataProvider>
    {/* <Router> */}
    <App />
    {/* </Router> */}
  </DataProvider>,
  document.getElementById('root')
);
